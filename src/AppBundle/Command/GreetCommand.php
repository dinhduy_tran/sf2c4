<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GreetCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:greet')
            ->setDescription('Say hello to someone')
            ->addArgument('who', InputArgument::REQUIRED, 'The name to greet')
            ->addOption('yell', 'y', InputOption::VALUE_NONE, 'Whether or not to yell')
        ;
    }

    public function isEnabled()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (OutputInterface::VERBOSITY_DEBUG === $output->getVerbosity()) {
            $output->writeln('[debug] debug mode!!!');
        }

        $message = 'Hello '.$input->getArgument('who').'!';
        if ($input->getOption('yell')) {
            $message = strtoupper($message);
        }

        $output->writeln($message);
    }
}
