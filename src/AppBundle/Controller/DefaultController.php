<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route(
     *   path="/hello/{name}",
     *   name="app_greet",
     *   requirements={
     *     "name"="[a-z-]+"
     *   },
     *   condition="request.headers.get('User-Agent') matches '/Firefox/i'"
     * )
     * @Method("GET")
     */
    public function greetAction($name = 'world')
    {
        return $this->render('default/greet.html.twig', [
            'name' => $name,
        ]);
    }

    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
}
